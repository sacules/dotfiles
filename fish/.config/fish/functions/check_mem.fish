function check_mem -d 'Get memory usage and send a notification'
    set ICON_THEME  "Papirus-Dark"
    set ICON_SIZE   "24x24@2x"
    set ICON_PATH   "/usr/share/icons/$ICON_THEME/$ICON_SIZE/panel/indicator-sensors-memory.svg"

    set mem_used 0
    set mem_total 0
    set tmp ""

    cat /proc/meminfo | while read -d ":" data val
            set tmp (string trim -c " kB" $val)

            switch $data
                case "MemTotal"
                    set mem_total $tmp
                    set mem_used (math $mem_used + $tmp)

                case "Shmem"
                    set mem_used (math $mem_used + $tmp)

                case "MemFree" "Buffers" "Cached" "SReclaimable"
                    set mem_used (math $mem_used - $tmp)
            end
        end

    set mem_used (math --scale=0 $mem_used / 1024)
    set mem_total (math --scale=0 $mem_total / 1024)

    notify-send --icon=$ICON_PATH "Memory" "$mem_used MB / $mem_total MB"
end
