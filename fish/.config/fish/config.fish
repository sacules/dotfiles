# Base16 Shell
if status --is-interactive
    set BASE16_SHELL "$HOME/.config/base16-shell/"
    source "$BASE16_SHELL/profile_helper.fish"
end

# Auto install fisher
if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

# Setup path properly
set -gx PATH /usr/local/bin /bin /usr/bin

# Set GOPATH properly
set -gx GOPATH ~/.go
set -gx PATH $PATH $GOPATH/bin

# Android studio bs
set -gx ANDROID_HOME ~/.local/bin/android-sdk

# Agda bs
set -gx AGDA_DIR ~/.agda

# Other stuff
set -gx PATH $PATH ~/.cargo/bin
set -gx PATH $PATH ~/.local/bin
set -gx PATH $PATH ~/.local/share/npm/bin
set -gx PATH $PATH ~/.cabal/bin
set -gx PATH $PATH /opt/texlive/2019/bin/x86_64-linux

set -gx QT_QPA_PLATFORMTHEME qt5ct

set -q XDG_RUNTIME_DIR; or set -gx XDG_RUNTIME_DIR /tmp/$USER-runtime-dir
if not test -d $XDG_RUNTIME_DIR
    mkdir $XDG_RUNTIME_DIR
    chmod 0700 $XDG_RUNTIME_DIR
end

set -gx CLASSPATH ~/.local/lib/antlr-4.7.1-complete.jar
alias antlr4="java -Xmx500M -cp '~/.local/lib/antlr-4.7.1-complete.jar:$CLASSPATH' org.antlr.v4.Tool"
alias grun="java -Xmx500M org.antlr.v4.runtime.misc.TestRig"

# pnpm
set -gx PNPM_HOME "/home/sacul/.local/share/pnpm"
if not string match -q -- $PNPM_HOME $PATH
  set -gx PATH "$PNPM_HOME" $PATH
end
# pnpm end
# bun
set --export BUN_INSTALL "$HOME/.bun"
set --export PATH $BUN_INSTALL/bin $PATH
