-- ┏━━━━━━━━━━━━━━━━━┓
-- ┃   Keybindings   ┃
-- ┗━━━━━━━━━━━━━━━━━┛

-- Better moving between windows
vim.keymap.set('n', '<C-h>', '<C-w>h')
vim.keymap.set('n', '<C-j>', '<C-w>j')
vim.keymap.set('n', '<C-k>', '<C-w>k')
vim.keymap.set('n', '<C-l>', '<C-w>l')

-- Use <F9> to clear the highlighting of :set hlsearch.
vim.keymap.set('n', '<F9>', ':noh<CR>')

-- Move better between tabs
vim.keymap.set('n', '<C-Right>', ':tabnext<CR>')
vim.keymap.set('n', '<C-Left>', ':tabprevious<CR>')

-- Change leader key to space
vim.g.mapleader = " "
