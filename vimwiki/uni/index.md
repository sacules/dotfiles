# Paradigmas
- [Filminas](https://sites.google.com/view/paradigmas2019/)
- Apuntes:
    - 14. [Programación Orientada a Objetos: Java](Programación Orientada a Objetos: Java)
    - 16. [Frameworks](Frameworks)
    - 17. [Scripting](Scripting)
    - 18. [Programación Lógica](Programación Lógica)
    - 19. [Programación Concurrente](Programación Concurrente)

# Redes
- [Aula Virtual](http://www.famaf.proed.unc.edu.ar/course/view.php?id=478)
- Apuntes
    - [Capa de red parte 2](Capa de red parte 2)
