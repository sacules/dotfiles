### Monitors

# Main Thinkpad display
monitor = eDP-1,preferred,0x1080,1 # dual screen
#monitor = eDP-1,preferred,0x2160,1 # dual 4k screen
#monitor = eDP-1,preferred,auto,1 # single
#monitor = eDP-1,disable

# External display
monitor = HDMI-A-1,1920x1080@60,0x0,1 # dual screen
#monitor = HDMI-A-1,1920x1080@60,0x0,1,bitdepth,10 # dual screen with 10 bit support
#monitor = HDMI-A-1,3840x2160@60,0x0,1 # dual 4k screen
#monitor = HDMI-A-1,3840x2160@60,0x0,2,bitdepth,10 # dual 4k screen with 10 bit support
#monitor = HDMI-A-1,1920x1080@60,auto,1,mirror,eDP-1 # mirrored
#monitor = HDMI-A-1,disable


### Workspaces

# Define where each display begins and ends
workspace = eDP-1,1
workspace = HDMI-A-1,11

# Map the workspaces to each display
workspace = 1,eDP-1
workspace = 2,eDP-1
workspace = 3,eDP-1
workspace = 4,eDP-1
workspace = 5,eDP-1
workspace = 6,eDP-1
workspace = 7,eDP-1
workspace = 8,eDP-1
workspace = 9,eDP-1
workspace = 10,HDMI-A-1
workspace = 11,HDMI-A-1
workspace = 12,HDMI-A-1
workspace = 13,HDMI-A-1
workspace = 14,HDMI-A-1
workspace = 15,HDMI-A-1
workspace = 16,HDMI-A-1
workspace = 17,HDMI-A-1
workspace = 18,HDMI-A-1
workspace = 19,HDMI-A-1
workspace = 20,HDMI-A-1
workspace = special:scratchpad, on-created-empty:kitty, gapsout:100

## Window rules

# Waydroid
windowrulev2 = float,class:^(waydroid.*|Waydroid)$
windowrulev2 = noborder,class:^(waydroid.*|Waydroid)$
windowrulev2 = size 375 667,class:^(waydroid.*|Waydroid)$


### Config
input {
    kb_layout=latam
    kb_variant=deadtilde
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
}

general {
    gaps_in = 5
    gaps_out = 20
    border_size = 3
	col.active_border = rgb(8FA1B3)
    col.inactive_border = rgb(434859)
    # col.active_border = rgb(f2e5bc)
    # col.inactive_border = rgb(79740e)
	no_border_on_floating = true

    layout = dwindle
}

decoration {
    rounding = 0

    drop_shadow = yes
    shadow_range = 4
    shadow_render_power = 3
    col.shadow = rgba(1a1a1aee)
}

animations {
    enabled = yes

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, default
}

dwindle {
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    new_is_master = false
	special_scale_factor = 0.7
}

gestures {
    workspace_swipe = off
}

device:epic mouse V1 {
    sensitivity = -0.5
}

### Keybinds

# Definitions
$mainMod = SUPER
$secondaryMod = SUPER_SHIFT

# Main binds
bind = $secondaryMod, RETURN, exec, kitty
bind = $secondaryMod, Q, exit, 
bind = $mainMod, X, killactive, 
bind = $mainMod, F, togglefloating, 
bind = $mainMod, R, exec, wofi --show drun
bind = $mainMod, T, pseudo, # dwindle
bind = $mainMod, BACKSPACE, togglespecialworkspace, special
bind = $mainMod, P, exec, bemenu-fancy.sh

# Move focus with mainMod + arrow keys
bind = $mainMod, H, movefocus, l
bind = $mainMod, L, movefocus, r
bind = $mainMod, J, movefocus, u
bind = $mainMod, K, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, exec, hyprsome workspace 1
bind = $mainMod, 2, exec, hyprsome workspace 2
bind = $mainMod, 3, exec, hyprsome workspace 3
bind = $mainMod, 4, exec, hyprsome workspace 4
bind = $mainMod, 5, exec, hyprsome workspace 5
bind = $mainMod, 6, exec, hyprsome workspace 6
bind = $mainMod, 7, exec, hyprsome workspace 7
bind = $mainMod, 8, exec, hyprsome workspace 8
bind = $mainMod, 9, exec, hyprsome workspace 9
bind = $mainMod, 0, exec, hyprsome workspace 10

# Toggle between the last workspace
bind = $mainMod, TAB, workspace, previous

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $secondaryMod, 1, exec, hyprsome move 1
bind = $secondaryMod, 2, exec, hyprsome move 2
bind = $secondaryMod, 3, exec, hyprsome move 3
bind = $secondaryMod, 4, exec, hyprsome move 4
bind = $secondaryMod, 5, exec, hyprsome move 5
bind = $secondaryMod, 6, exec, hyprsome move 6
bind = $secondaryMod, 7, exec, hyprsome move 7
bind = $secondaryMod, 8, exec, hyprsome move 8
bind = $secondaryMod, 9, exec, hyprsome move 9
bind = $secondaryMod, 0, exec, hyprsome move 10

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow


### Misc

misc {
	disable_hyprland_logo = true
}

# Some default env vars.
env = XCURSOR_SIZE,24

# Startup
exec-once=~/Dotfiles/scripts/wayland-start.fish
exec-once=dbus-update-activation-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
