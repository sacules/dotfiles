#!/usr/bin/env bash

# Print bold characters
bold=$(tput bold)
normal=$(tput sgr0)

echo "${bold}==> Removing stupid crap${normal}"
sudo rm -frv /var/service/agetty-tty{3,4,5,6}

echo "${bold}==> Installing your crap${normal}"
sudo xbps-install -Suvy fish-shell htop xorg-minimal xinit xf86-video-intel neofetch make base-devel curl scrot autoconf automake noto-fonts-ttf{,-extra} tar bzip2 xz zip unzip p7zip alsa-utils alsa-tools xtools ranger python3 w3m w3m-img libexif-devel imlib2-devel giflib-devel compton xset xsetroot xrandr setxkbmap acpi udiskie firefox courier-unicode lxappearance Adapta qt5ct libXft-devel libXinerama-devel papirus-icon-theme inxi telegram-desktop ffmpegthumbnailer poppler-utils clang qbittorrent gsmartcontrol feh pulseaudio pavucontrol ntfs-3g cava tlp tpacpi-bat tp_smapi-dkms xdg-utils cmatrix rsync mpv hddtemp lm_sensors void-repo-nonfree gimp godot zathura zathura-{cb,pdf-mupdf} liberation-fonts-ttf xrdb git dunst wget gparted stow xclip go

echo "${bold}==> Preparing for automount${normal}"
echo 'tmpfs /media tmpfs nodev,nosuid,size=1M 0 0' | sudo tee --append /etc/fstab
