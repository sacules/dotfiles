* Packages: Namespaces, forma de juntar clases y variables, una propiedad en común
* Java innova la idea de laburar con la red
* Diseñado para poder incorporar gente que no es experta en programación
* Pasaje de parametros es por valor, pero el valor es la referencia
* Los campos estáticos de la clase se inicializan no en tiempo de compilación ni ejecución, sino en tiempo de carga
* `Finalize` no libera memoria, sino los recursos asociados al mismo
* Todo encapsulado en clases
* Herencia: no múltiple, pero casi - usa la palabra `extends`

```java
class ColorPoint extends Point {
    // No se requiere especificar el nivel de visibilidad, pero en general
    // una IDE tira warnings sino - es una práctica bastante común
    private Color c;
    //...
}
```

* El código se compila para Java o llega compilado por la red, hasta la Java Virtual Machine
* Seguridad opera de manera parecida a cómo lo hace un sistema operativo - parte de la implementación del lenguaje
