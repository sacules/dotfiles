function shot --wraps='maim -s -u -d 3 | xclip -selection clipboard -t image/png' --description 'screenshot with maim'
  maim -s -u -d 3 | xclip -selection clipboard -t image/png $argv; 
end
