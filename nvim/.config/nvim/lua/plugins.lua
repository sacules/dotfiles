-- Enable plugins
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- ┏━━━━━━━━━━━━━┓
-- ┃   Plugins   ┃
-- ┗━━━━━━━━━━━━━┛
require("lazy").setup({
    -- Automatically close pairs
    "windwp/nvim-autopairs",

    -- SublimeText-like multiple selection
    'terryma/vim-multiple-cursors',

    -- Base 16 colors
    { "RRethy/nvim-base16" },

    -- Fancy icons
    "kyazdani42/nvim-web-devicons",

    -- Status line
    "nvim-lualine/lualine.nvim",

    -- Language parser
    { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },

    -- Comment toggler
    "numToStr/Comment.nvim",

    -- Add, modify, or delete parenthesis, brackets, quotes, etc.
    "tpope/vim-surround",

    -- Fancy file tree
    "nvim-tree/nvim-tree.lua",

    -- Search files, buffers, commits, etc
    {
        'nvim-telescope/telescope.nvim',
        dependencies = { 'nvim-lua/plenary.nvim' }
    },

    -- Git wrapper
    'tpope/vim-fugitive',

    -- Add git glyphs on the gutter column
    "lewis6991/gitsigns.nvim",

    -- LSP config
    "neovim/nvim-lspconfig",

    -- LSP completion
    "hrsh7th/nvim-cmp",
    "hrsh7th/cmp-buffer",
    "hrsh7th/cmp-path",
    "saadparwaiz1/cmp_luasnip",
    "hrsh7th/cmp-nvim-lsp",
    "L3MON4D3/LuaSnip",
    "rafamadriz/friendly-snippets",

    -- LSP status
    "nvim-lua/lsp-status.nvim",

    -- LSP diagnostics
    "folke/trouble.nvim",

    -- LSP fixing / linting
    "jose-elias-alvarez/null-ls.nvim",

    -- LSP function signature
    "ray-x/lsp_signature.nvim",

    -- LSP config for Rust
    "simrat39/rust-tools.nvim",

    -- Debugging
    "nvim-lua/plenary.nvim",
    "mfussenegger/nvim-dap",
    "rcarriga/nvim-dap-ui",

    -- Tidal
    "tidalcycles/vim-tidal"
})

-- ┏━━━━━━━━━━━━━━━━━━━━┓
-- ┃   nvim-autopairs   ┃
-- ┗━━━━━━━━━━━━━━━━━━━━┛
require('nvim-autopairs').setup({})

-- ┏━━━━━━━━━━━━━━━━━┓
-- ┃   nvim-base16   ┃
-- ┗━━━━━━━━━━━━━━━━━┛
--vim.cmd('colorscheme base16-gruvbox-light-soft')
vim.cmd('colorscheme base16-ocean')


-- ┏━━━━━━━━━━━━━┓
-- ┃   Lualine   ┃
-- ┗━━━━━━━━━━━━━┛
require('lualine').setup({
    options = {
        theme = 'base16',
        icons_enabled = false,
        component_separators = '|',
        section_separators = '',
        disabled_filetypes = {
            statusline = { 'NvimTree' }
        },
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'filename' },
        lualine_c = { { 'branch', icons_enabled = true }, 'diff', 'diagnostics' },
        lualine_x = { "require('lsp-status').status()" },
        lualine_y = {
            { 'filetype', fmt = string.upper }
        },
        lualine_z = { 'location' }
    }
})


-- ┏━━━━━━━━━━━━━━━━┓
-- ┃   Treesitter   ┃
-- ┗━━━━━━━━━━━━━━━━┛
require('nvim-treesitter.configs').setup({
    highlight = {
        enable = true,
    },
    ensure_installed = {
        'javascript',
        'typescript',
        'tsx',
        'css',
        'json',
        'lua',
        'go',
        'markdown'
    },
})

vim.keymap.set("n", "<C-e>",
    function()
        local result = vim.treesitter.get_captures_at_cursor(0)
        print(vim.inspect(result))
    end,
    { noremap = true, silent = false }
)

-- ┏━━━━━━━━━━━━━━━┓
-- ┃   nvim-tree   ┃
-- ┗━━━━━━━━━━━━━━━┛
require("nvim-tree").setup()

-- Open it with F10
vim.keymap.set('n', '<F10>', '<CMD>NvimTreeToggle lsp_diagnostics<CR>')

-- ┏━━━━━━━━━━━━━━━┓
-- ┃   Telescope   ┃
-- ┗━━━━━━━━━━━━━━━┛
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>f', builtin.find_files, {})
vim.keymap.set('n', '<leader>r', builtin.live_grep, {})
vim.keymap.set('n', '<leader>b', builtin.buffers, {})
vim.keymap.set('n', '<leader>h', builtin.help_tags, {})

-- ┏━━━━━━━━━━━━━━┓
-- ┃   gitsigns   ┃
-- ┗━━━━━━━━━━━━━━┛
require('gitsigns').setup({
  signs = {
    add = {text = '┃'},
    change = {text = '┃'},
    delete = {text = '◢'},
    topdelete = {text = '◥'},
    changedelete = {text = '◢'},
  }
})

-- ┏━━━━━━━━━┓
-- ┃   LSP   ┃
-- ┗━━━━━━━━━┛

-- Config client's capabilities
local lspconf = require('lspconfig')
local lspdef = lspconf.util.default_config

lspdef.capabilities = vim.tbl_deep_extend(
  'force',
  lspdef.capabilities,
  require('cmp_nvim_lsp').default_capabilities()
)

-- Setup keybindings
vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'LSP actions',
  callback = function()
    local bufmap = function(mode, lhs, rhs)
      local opts = { buffer = true }
      vim.keymap.set(mode, lhs, rhs, opts)
    end

    -- Displays hover information about the symbol under the cursor
    bufmap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>')

    -- Renames all references to the symbol under the cursor
    bufmap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<cr>')

    -- Selects a code action available at the current cursor position
    bufmap('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>')
    bufmap('x', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>')

    -- Move to the previous diagnostic
    bufmap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>')

    -- Move to the next diagnostic
    bufmap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>')
  end
})

-- Snippets
require('luasnip.loaders.from_vscode').lazy_load()

-- Autocompletion
local cmp = require('cmp')
local luasnip = require('luasnip')

local select_opts = {behavior = cmp.SelectBehavior.Select}

cmp.setup({
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end
  },
  sources = {
    {name = 'path'},
    {name = 'nvim_lsp', keyword_length = 1},
    {name = 'buffer', keyword_length = 3},
    {name = 'luasnip', keyword_length = 2},
  },
  window = {
    documentation = cmp.config.window.bordered()
  },
  formatting = {
    fields = {'menu', 'abbr', 'kind'},
    format = function(entry, item)
      local menu_icon = {
        nvim_lsp = 'λ',
        luasnip = '⋗',
        buffer = 'Ω',
        path = '🖫',
      }

      item.menu = menu_icon[entry.source.name]
      return item
    end,
  },
  mapping = {
    ['<Up>'] = cmp.mapping.select_prev_item(select_opts),
    ['<Down>'] = cmp.mapping.select_next_item(select_opts),

    ['<C-p>'] = cmp.mapping.select_prev_item(select_opts),
    ['<C-n>'] = cmp.mapping.select_next_item(select_opts),

    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),

    ['<C-e>'] = cmp.mapping.abort(),
    ['<C-y>'] = cmp.mapping.confirm({select = true}),
    ['<CR>'] = cmp.mapping.confirm({select = false}),

    ['<C-f>'] = cmp.mapping(function(fallback)
      if luasnip.jumpable(1) then
        luasnip.jump(1)
      else
        fallback()
      end
    end, {'i', 's'}),

    ['<C-b>'] = cmp.mapping(function(fallback)
      if luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {'i', 's'}),

    ['<Tab>'] = cmp.mapping(function(fallback)
      local col = vim.fn.col('.') - 1

      if cmp.visible() then
        cmp.select_next_item(select_opts)
      elseif col == 0 or vim.fn.getline('.'):sub(col, col):match('%s') then
        fallback()
      else
        cmp.complete()
      end
    end, {'i', 's'}),

    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item(select_opts)
      else
        fallback()
      end
    end, {'i', 's'}),
  },
})

-- Diagnostics
vim.diagnostic.config({
  virtual_text = false,
  severity_sort = true,
  signs = false,
  float = {
    border = 'rounded',
    source = 'always',
  },
})

require("trouble").setup({
    auto_open = true,
    auto_close = true,
	mode = "document_diagnostics",
	height = 5,
})

-- List definitions
vim.keymap.set("n", "<F8>", "<cmd>TroubleToggle<cr>",
  {silent = true, noremap = true}
)
vim.keymap.set("n", "gr", "<cmd>TroubleToggle lsp_references<cr>",
  {silent = true, noremap = true}
)
vim.keymap.set("n", "gd", "<cmd>TroubleToggle lsp_definitions<cr>",
  {silent = true, noremap = true}
)

-- Borders on help windows
vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
  vim.lsp.handlers.hover,
  {border = 'rounded'}
)

vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
  vim.lsp.handlers.signature_help,
  {border = 'rounded'}
)

-- Don't spam with diagnostics while I'm typing
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
    update_in_insert = false,
  }
)

-- Language servers
local lsp_status = require('lsp-status')
lsp_status.register_progress()
lsp_status.config({
    indicator_ok = '',
})

-- Show popup window faster
vim.o.updatetime = 250

local _on_attach = function(client, bufnr)
    lsp_status.on_attach(client, bufnr)

    -- Show diagnostics under cursor on popup window
    vim.api.nvim_create_autocmd("CursorHold", {
        buffer = bufnr,
        callback = function()
            local opts = {
                focusable = false,
                close_events = { "BufLeave", "CursorMoved", "InsertEnter", "FocusLost" },
                border = 'rounded',
                source = 'always',
                prefix = ' ',
                scope = 'cursor',
            }
            vim.diagnostic.open_float(nil, opts)
        end
    })

	-- Fix coloring
	--vim.api.nvim_set_hl(0, "@type", { link = "Operator" })
	--vim.api.nvim_set_hl(0, "@type.builtin", { link = "Type" })
	vim.api.nvim_set_hl(0, "@function", { link = "Ignore" })
	vim.api.nvim_set_hl(0, "@function.builtin", { link = "Keyword" })
	--vim.api.nvim_set_hl(0, "@constant", { link = "Operator" })
	--vim.api.nvim_set_hl(0, "@parameter", { link = "Operator" })
	--vim.api.nvim_set_hl(0, "@property", { link = "Operator" })
	vim.api.nvim_set_hl(0, "@method.call", { link = "Ignore" })
	vim.api.nvim_set_hl(0, "@variable", { link = "Ignore" })
	--vim.api.nvim_set_hl(0, "@variable.field", { link = "Operator" })
	--vim.api.nvim_set_hl(0, "@variable.builtin", { link = "Float" })
	--vim.api.nvim_set_hl(0, "@field", { link = "Operator" })
	vim.api.nvim_set_hl(0, "@function.call", { link = "Ignore" })
	--vim.api.nvim_set_hl(0, "@punctuation.delimiter", { link = "Operator" })
	--vim.api.nvim_set_hl(0, "@punctuation.bracket", { link = "Operator" })
	--vim.api.nvim_set_hl(0, "@tag.delimiter", { link = "Operator" })

	-- Nicer separator
	vim.api.nvim_set_hl(0, "WinSeparator", { link = "Comment" })
end

local on_attach = function(client, bufnr)
	-- Prevent LSP from highlighting instead of tree-sitter
	client.server_capabilities.semanticTokensProvider = nil

	_on_attach(client, bufnr)
end

lspconf.tsserver.setup({
    on_attach = on_attach,
	flags = {
      debounce_text_changes = 150,
    },
})

lspconf.ccls.setup({
    on_attach = on_attach,
    init_options = {
        compilationDatabaseDirectory = ".",
        index = {
            threads = 0,
        },
        cache = {
            directory = ".ccls-cache"
        }
    },
})

lspconf.gopls.setup({
    on_attach = on_attach
})

lspconf.templ.setup({
    on_attach = on_attach,
	flags = {
      debounce_text_changes = 150,
    },
})

lspconf.astro.setup({
    on_attach = on_attach
})

lspconf.lua_ls.setup({
    on_attach = on_attach,
	on_init = function(client)
		local path = client.workspace_folders[1].name
		if not vim.loop.fs_stat(path..'/.luarc.json') and not vim.loop.fs_stat(path..'/.luarc.jsonc') then
		  client.config.settings = vim.tbl_deep_extend('force', client.config.settings.Lua, {
			runtime = {
			  -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
			  version = 'LuaJIT'
			},
			-- Make the server aware of Neovim runtime files
			workspace = {
			  library = { vim.env.VIMRUNTIME }
			  -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
			  -- library = vim.api.nvim_get_runtime_file("", true)
			}
		  })

		  client.notify("workspace/didChangeConfiguration", { settings = client.config.settings })
		end
		return true
	end
})

-- Linting / fixing
local null_ls = require("null-ls")

local augroup = vim.api.nvim_create_augroup("LspFormatting", {})
null_ls.setup({
    sources = {
		-- Diagnostics
        null_ls.builtins.diagnostics.eslint_d,

		-- Formatting
        null_ls.builtins.formatting.prettierd,
        null_ls.builtins.formatting.gofmt,
        null_ls.builtins.formatting.rustfmt.with({
            extra_args = { "--edition=2021" }
        }),
    },
    -- Format on save
    on_attach = function(client, bufnr)
        if client.supports_method("textDocument/formatting") then
            vim.api.nvim_clear_autocmds({ group = augroup, buffer = bufnr })
            vim.api.nvim_create_autocmd("BufWritePre", {
                group = augroup,
                buffer = bufnr,
                callback = function()
                    vim.lsp.buf.format({ bufnr = bufnr })
                end,
            })
        end
    end,
})

-- Show function signature while typing
require("lsp_signature").setup({
    hint_enable = false
})

-- Rust-specific tools
local rt = require("rust-tools")

rt.setup({
    server = {
        on_attach = on_attach,
        settings = {
            ["rust-analyzer"] = {
                diagnostics = {
                    disabled = { "unresolved-proc-macro" }
                }
            }
        }
    },
    inlay_hints = {
        auto = false
    }
})


-- ┏━━━━━━━━━┓
-- ┃   DAP   ┃
-- ┗━━━━━━━━━┛
require("dap").adapters["pwa-node"] = {
  type = "server",
  host = "localhost",
  port = "${port}",
  executable = {
    command = "node",
    args = {"/home/sacul/Downloads/js-debug/src/dapDebugServer.js", "${port}"},
  }
}

local dap_utils = require("dap.utils")

require("dap").configurations.typescript = {
    {
        type = "pwa-node",
        request = "attach",
        name = "Attach Program (pwa-node, select pid)",
        cwd = vim.fn.getcwd(),
        processId = dap_utils.pick_process,
        skipFiles = { "<node_internals>/**" },
    }
}

require("dapui").setup()
